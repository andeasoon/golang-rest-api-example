# Environment
* AWS EC2(Red Hat Enterprise Linux release 8.2 (Ootpa))
* AWS RDS(MariaDB)

# AWS Config
* https://victorydntmd.tistory.com/61
* https://victorydntmd.tistory.com/337

# EC2 Setting
* sudo su
* yum -y update
* yum -y isntall wget
* yum -y install git
* yum -y install golang
* wget https://downloads.mariadb.com/MariaDB/mariadb_repo_setup
* chmod +x mariadb_repo_setup
* sudo ./mariadb_repo_setup
* yum -y install MariaDB-client
* dnf module install redis
* systemctl start redis

# Package Install
* useradd golang-rest-api-user
* su - golang-rest-api-user
* git clone https://gitlab.com/andeasoon/golang-rest-api-example.git
* export GOROOT=/usr/lib/golang
* export GOPATH=$HOME/golang-rest-api-example
* export PATH=$PATH:$HOME/golang-rest-api-example/bin
* echo -e "\nexport GOROOT=/usr/lib/golang\nexport GOPATH=\\$HOME/golang-rest-api-example\nexport PATH=\\$PATH:\\$HOME/golang-rest-api-example/bin" >> ~/.bash_profile

# Module Install
* go get -u github.com/go-sql-driver/mysql
* go get -u github.com/gin-gonic/gin
* go get -u github.com/alecthomas/template
* go get -u github.com/swaggo/gin-swagger
* go get -u github.com/swaggo/gin-swagger/swaggerFiles
* go get -u github.com/swaggo/swag/cmd/swag
* go get -u github.com/gomodule/redigo
* go get -u github.com/gocelery/gocelery
* swag init

# Database schema
* mysql -h \<hostname\> -u admin -p
* use test
> CREATE TABLE `user` (
>   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
>   `email` varchar(256),
>   `name` varchar(256),
>   `influencer` char(1) DEFAULT 'f',
>   `created_at` datetime DEFAULT '0000-00-00 00:00:00',
>   PRIMARY KEY (`id`),
>   UNIQUE KEY `IDX_user_email` (`email`),
>   KEY `IDX_user_name` (`name`),
>   KEY `IDX_user_influencer` (`influencer`),
>   KEY `IDX_user_created_at` (`created_at`)
> ) ENGINE=INNODB DEFAULT CHARSET='utf8';

> CREATE TABLE `follower` (
>   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
>   `user_id` int(10) unsigned,
>   `follower_user_id` int(10) unsigned,
>   `created_at` datetime DEFAULT '0000-00-00 00:00:00',
>   PRIMARY KEY (`id`),
>   KEY `IDX_follower_user_id` (`user_id`),
>   KEY `IDX_follower_created_at` (`created_at`),
>   FOREIGN KEY (`user_id`) 
>   REFERENCES user(`id`) ON UPDATE CASCADE ON DELETE RESTRICT
> ) ENGINE=INNODB DEFAULT CHARSET='utf8';

> CREATE TABLE `post` (
>   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
>   `user_id` int(10) unsigned NOT NULL,
>   `contents` text NOT NULL,
>   `tags` text,
>   `locale` varchar(256) NOT NULL DEFAULT '',
>   `created_at` datetime DEFAULT '0000-00-00 00:00:00',
>   PRIMARY KEY (`id`),
>   KEY `IDX_post_user_id` (`user_id`),
>   KEY `IDX_post_locale` (`locale`),  
>   KEY `IDX_post_created_at` (`created_at`),
>   FOREIGN KEY (`user_id`) 
>   REFERENCES user(`id`) ON UPDATE CASCADE ON DELETE RESTRICT
> ) ENGINE=INNODB DEFAULT CHARSET='utf8';

# Data Insert
* insert into user (email, name, influencer, created_at) values ('user2@test.com', 'user2', 'f', now());
* insert into user (email, name, influencer, created_at) values ('user3@test.com', 'user3', 'f', now());
* insert into user (email, name, influencer, created_at) values ('user4@test.com', 'user4', 'f', now());
* insert into user (email, name, influencer, created_at) values ('user5@test.com', 'user5', 'f', now());
* insert into user (email, name, influencer, created_at) values ('user6@test.com', 'user6', 'f', now());
* insert into user (email, name, influencer, created_at) values ('user7@test.com', 'user7', 't', now());
* insert into user (email, name, influencer, created_at) values ('user8@test.com', 'user8', 'f', now());
* insert into user (email, name, influencer, created_at) values ('user9@test.com', 'user9', 't', now());
* insert into user (email, name, influencer, created_at) values ('user10@test.com', 'user10', 'f', now());
* insert into follower (user_id, follower_user_id, created_at) values (7, 1, now());
* insert into follower (user_id, follower_user_id, created_at) values (7, 2, now());
* insert into follower (user_id, follower_user_id, created_at) values (7, 3, now());
* insert into follower (user_id, follower_user_id, created_at) values (7, 4, now());
* insert into follower (user_id, follower_user_id, created_at) values (7, 5, now());
* insert into follower (user_id, follower_user_id, created_at) values (7, 6, now());
* insert into follower (user_id, follower_user_id, created_at) values (7, 8, now());
* insert into follower (user_id, follower_user_id, created_at) values (7, 9, now());
* insert into follower (user_id, follower_user_id, created_at) values (7, 10, now());
* insert into follower (user_id, follower_user_id, created_at) values (9, 1, now());
* insert into follower (user_id, follower_user_id, created_at) values (9, 2, now());
* insert into follower (user_id, follower_user_id, created_at) values (9, 3, now());
* insert into follower (user_id, follower_user_id, created_at) values (9, 4, now());
* insert into follower (user_id, follower_user_id, created_at) values (9, 5, now());
* insert into follower (user_id, follower_user_id, created_at) values (9, 6, now());
* insert into post (user_id, contents, tags, locale, created_at) value (7, 'Got is love', '#Got#love', 'heaven', now());

# Run
* su - golang-rest-api-user
* go build
* ./golang-rest-api-example

# Test
* http://instance.public.domain:8080/swagger/index.html