// main.go
package main

import (
	"bufio"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"
	"reflect"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gocelery/gocelery"

	//"github.com/go-redis/redis"
	"github.com/gomodule/redigo/redis"
	ginSwagger "github.com/swaggo/gin-swagger"
	swaggerFiles "github.com/swaggo/gin-swagger/swaggerFiles"
	"gitlab.com/andeasoon/golang-rest-api-example/docs"
)

type User struct {
	name string `json:"name"`
	age  int    `json:"age"`
}

type welcomeModel struct {
	ID   int    `json:"id" example:"1" format:"int64"`
	Name string `json:"name" example:"account name"`
}

func setupRouter() *gin.Engine {

	r := gin.Default()

	r.GET("/welcome/:name", welcomePathParam)
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}

// Welcome godoc
// @Summary Summary를 적어 줍니다.
// @Description 자세한 설명은 이곳에 적습니다.
// @name get-string-by-int
// @Accept  json
// @Produce  json
// @Param name path string true "User name"
// @Router /welcome/{name} [get]
// @Success 200 {object} welcomeModel
func welcomePathParam(c *gin.Context) {
	name := c.Param("name")
	message := name + " is very handsome!"
	welcomeMessage := welcomeModel{1, message}

	c.JSON(200, gin.H{"message": welcomeMessage})
}

func main() {
	_name := "Gopher"
	_age := 7
	fmt.Printf("name: %s\n", _name)
	fmt.Printf("age: %d\n", _age)
	u := User{_name, _age}
	fmt.Printf("%v\n", u)
	fmt.Printf("%+v\n", u)
	fmt.Printf("%#v\n", u)
	u2 := &User{name: "Gopher", age: 7}
	b, err := json.Marshal(u2)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(b)
	fmt.Println(string(b))

	f, _ := os.Create("C:\\Temp\\test.txt")
	w := bufio.NewWriter(f)
	fmt.Fprint(w, "Hello World\n")
	w.Flush()

	db, err := sql.Open("mysql", "root:1234@tcp(127.0.0.1:3306)/test")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var id int
	var influencer string
	rows, err := db.Query("SELECT u.id, u.influencer FROM follower AS f JOIN user AS u ON u.id=f.user_id WHERE f.follower_user_id=? AND u.influencer='t'", 1)
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&id, &influencer)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(id, influencer)
	}

	/*
		client := redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "",
			DB:       0,
		})

		pong, err := client.Ping(client.Context()).Result()
		fmt.Println(pong, err)
	*/

	// create redis connection pool
	redisPool := &redis.Pool{
		Dial: func() (redis.Conn, error) {
			c, err := redis.DialURL("redis://")
			if err != nil {
				return nil, err
			}
			return c, err
		},
	}

	conn := redisPool.Get()
	fmt.Println(conn.Do("PING"))

	// initialize celery client
	cli, _ := gocelery.NewCeleryClient(
		gocelery.NewRedisBroker(redisPool),
		&gocelery.RedisCeleryBackend{Pool: redisPool},
		1,
	)

	// prepare arguments
	taskName := "worker.add"
	argA := rand.Intn(100)
	argB := rand.Intn(100)
	log.Printf("argA: %d", argA)
	log.Printf("argB: %d", argB)

	// run task
	asyncResult, err := cli.Delay(taskName, argA, argB)
	if err != nil {
		panic(err)
	}
	work()

	// get results from backend with timeout
	res, err := asyncResult.Get(10 * time.Second)
	if err != nil {
		panic(err)
	}

	log.Printf("result: %+v of type %+v", res, reflect.TypeOf(res))

	/*
		err = client.Set(client.Context(), "name", "Elliot", 0).Err()
		if err != nil {
			fmt.Println(err)
		}

		val, err := client.Get(client.Context(), "name").Result()
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(val)
	*/

	/*
	  r := gin.Default()
	  r.GET("/ping", func(c *gin.Context) {
	    c.JSON(200, gin.H{
	      "message": "pong",
	    })
	  })
	*/
	// programatically set swagger info
	docs.SwaggerInfo.Title = "Swagger Example API"
	docs.SwaggerInfo.Description = "This is a sample server for Swagger."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.BasePath = ""
	r := setupRouter()
	r.Run() // listen and serve on 0.0.0.0:8080
}
